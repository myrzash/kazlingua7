class Test {
  String task;
  List<String> answers;

  Test({this.task, this.answers});

  Test.fromJSON(json) {
    task = json["task"];
    task = task.trim();
    List answers = json["answers"];
    this.answers = answers == null
        ? []
        : answers
            .map((json) =>
                json.toString().trim().replaceAll(new RegExp(r"\s+"), " "))
            .toList();
  }
}
