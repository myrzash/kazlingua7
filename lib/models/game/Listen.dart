class Listen {
  String kz;
  String sound;

  Listen({this.kz, this.sound});

  Listen.fromJSON(json)
      : kz = json['kz'],
        sound = json['sound'];
}
