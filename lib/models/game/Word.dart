class Word {
  String kz;
  String image;

  Word({this.kz, this.image});

  Word.fromJSON(json)
      : kz = json["kz"],
        image = json["image"];
}
