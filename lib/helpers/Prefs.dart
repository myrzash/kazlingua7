import 'package:shared_preferences/shared_preferences.dart';

class Prefs {
  static final Prefs shared = Prefs();

  Future<int> getResult(String key) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  Future<void> saveResult(String key, int value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(key, value);
  }
}
